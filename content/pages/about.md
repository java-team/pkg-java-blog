Title: About

This is the official blog of the Debian Java Packaging Team.

This blog is powered by [Pelican][pelican]. You can find the templates and
theme used on this blog at [git.debian.org][gitdo]. Patches, new themes
proposals and constructive suggestions are welcome!

This site is under the same license and copyright as the [Debian][debian]
website, see [Debian WWW pages license][wwwlicense].

If you want to contact us, please send an email to the [debian-java
mailing list][debian-java]. This is a publically archived list.


[pelican]: http://getpelican.com/ "Find out about Pelican"
[debian]: http://www.debian.org "Debian - The Universal Operating System"
[gitdo]: http://anonscm.debian.org/cgit/publicity/bits.git
[wwwlicense]: http://www.debian.org/license
[debian-java]: https://lists.debian.org/debian-java/
