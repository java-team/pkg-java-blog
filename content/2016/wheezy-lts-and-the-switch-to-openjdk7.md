Title: Wheezy LTS and the switch to OpenJDK 7
Date: 2016-06-19
Authors: Markus Koschany
Status: published

Wheezy's LTS period started a few weeks ago and the LTS team had to make an
early support decision concerning the Java eco-system since Wheezy ships two Java
runtime environments [OpenJDK 6](https://tracker.debian.org/pkg/openjdk-6) and
[OpenJDK 7](https://tracker.debian.org/pkg/openjdk-7). (To be fair, there are
actually three but [gcj](https://packages.debian.org/oldstable/gcj-4.6-jre) has
been superseded by OpenJDK a long time ago and the latter should be preferred
whenever possible.)

OpenJDK 6 is currently maintained by Red Hat and we mostly rely on their
upstream work as well as on package updates from Debian's maintainer Matthias Klose and
Tiago Stürmer Daitx from Ubuntu. We already knew that both intend
to support OpenJDK 6 until April 2017 when Ubuntu 12.04 will reach its
end-of-life. Thus we had basically two options, supporting OpenJDK 6 for another
twelve months or dropping support right from the start. One of my first steps
was to ask for feedback and advice on
[debian-java](https://lists.debian.org/debian-java/2016/03/msg00044.html) since
supporting only one JDK seemed to be the more reasonable solution. We agreed on warning users
via various channels about the intended change, especially about [possible
incompatibilities](http://www.oracle.com/technetwork/java/javase/compatibility-417013.html)
with OpenJDK 7. Even Andrew Haley, OpenJDK 6 project lead, [participated](https://lists.debian.org/debian-java/2016/03/msg00055.html) in the discussion and confirmed that, while still supported, OpenJDK 6 security
releases are "always the last in the queue when there is urgent work to be
done".

I informed [debian-lts](https://lists.debian.org/debian-lts/2016/03/msg00098.html) about
my findings and issued a [call for tests](https://lists.debian.org/debian-lts/2016/04/msg00070.html) later.

Eventually we decided to [concentrate our efforts on OpenJDK
7](https://www.debian.org/News/2016/20160425) because we are confident that for the
majority of our users one Java implementation is sufficient during a stable
release cycle. An immediate positive effect in making OpenJDK 7 the default is that
resources can be relocated to more pressing issues. On the other hand we were
also forced to make compromises. The switch to a newer default implementation
usually triggers a major transition with dozens of FTBFS bugs and the [OpenJDK 7
transition](https://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=openjdk-7-transition;users=ubuntu-devel@lists.ubuntu.com)
was no exception. I pondered about the usefulness of fixing all these bugs for
Wheezy LTS again and focussing on runtime issues instead and finally decided
that the latter was both more reasonable and more economic.

Different from regular default Java changes, users will still be able to use OpenJDK 6 to
compile their packages and the security impact for development systems is in general neglectable.
More important was to avoid runtime installations of OpenJDK 6. I identified eighteen
packages that strictly depended on the now obsolete JRE and [fixed those
issues](https://lists.debian.org/debian-lts-announce/2016/05/msg00007.html)
on 4 May 2016 together with an update of
[java-common](https://tracker.debian.org/pkg/java-common) and announced the switch to
OpenJDK 7 with a Debian NEWS file.

If you are not a regular reader of [Debian
news](https://www.debian.org/News/news) and also not subscribed to
[debian-lts](https://lists.debian.org/debian-lts/),
[debian-lts-announce](https://lists.debian.org/debian-lts-announce/) or
[debian-java](https://lists.debian.org/debian-java/), remember **26 June
2016** is the day when OpenJDK 7 will be made the default Java implementation in
Wheezy LTS. Of course there is no need to wait. You can switch right now:

```
sudo update-alternatives --config java
```

