Title: What's new since Jessie?
Date: 2016-05-04
Authors: Emmanuel Bourg, Markus Koschany, Tony Mancill
Status: published

Jessie was released one year ago now and the Java Team has been busy preparing
the next release. Here is a quick summary of the current state of the Java packages:

 * A total of 136 packages have been added, 63 removed, 213 upgraded to a new
   upstream release, and 145 updated. We are now maintaining 892 packages (+12.34%).
 * OpenJDK 8 is now the default Java runtime in testing/unstable. OpenJDK 7
   has been removed, as well as several packages that couldn't be upgraded
   to work with OpenJDK 8 (avian, eclipse).
 * OpenJDK 9 is available in experimental. As a reminder, it won't be part
   of the next release; OpenJDK 8 will be the only Java runtime supported
   for Stretch.
 * Netbeans didn't make it into Jessie, but it is now back and up to date.
 * The main build tools are close to their latest upstream releases, especially
   Maven and Gradle which were seriously lagging behind.
 * Scala has been upgraded to the version 2.11. We are looking for Scala experts
   to maintain the package and its dependencies.
 * Freemind has been removed due to lack of maintenance, Freeplane is recommended instead.
 * The [reproducibility](https://reproducible.debian.net/unstable/amd64/pkg_set_maint_pkg-java-maintainers.html)
   rate has greatly improved, climbing from 50% to 75% in the past year.
 * Backports are continuously provided for the key packages and applications: OpenJDK 8, OpenJFX,
   Ant, Maven, Gradle, Tomcat 7 & 8, Jetty 8 & 9, OpenJDK 8, OpenJFX, jEdit.
 * The transition to Maven 3 has been completed, and packages are no longer
   built with Maven 2.
 * We replaced several obsolete libraries and transitioned them to their latest
   versions - for example, asm2, commons-net1 and commons-net2. Groovy 1.x was replaced
   with Groovy 2, and we upgraded BND, an important tool to develop with OSGi, and
   more than thirty of its reverse-dependencies from the 1.x series to version 2.4.1.
 * New packaging tools have been created to work with Gradle ([gradle-debian-helper](https://tracker.debian.org/pkg/gradle-debian-helper))
   and Ivy ([ivy-debian-helper](https://tracker.debian.org/pkg/ivy-debian-helper)).

## Outlook, goals and request for help

 * We have several difficult transitions ahead: BND 3, Tomcat 7 to 8, Jetty 8 to 9,
   ASM 5, and of course Java&nbsp;9. Any help would be welcome.
 * [Eclipse](https://tracker.debian.org/pkg/eclipse) is severely outdated and currently not part of testing. We would
   like to update this important piece of software and its corresponding
   modules to the latest upstream release, but we need more active people who
   want to maintain them. If you care about the Eclipse ecosystem,
   please [get in touch with us](https://lists.debian.org/debian-java/).
 * We still are in the midst of removing [old libraries](https://wiki.debian.org/Java/Oldlibs)
   like asm3, commons-httpclient and the servlet 2.5 API, which is part of the
   Tomcat 6 source package.
 * Want to see [Azureus/Vuze](https://tracker.debian.org/pkg/azureus) in
   Stretch again? Packaging is almost complete but we are looking for someone who
   can clarify remaining licensing issues with upstream and wants to maintain the
   software for the foreseeable future.
 * Do you have more ideas and want to get involved with the Java Team? Just send
   your suggestions to debian-java@lists.debian.org or chat with us on IRC at
   irc.debian.org, #debian-java.

## Java and Friends

 * The Java Team is not the only team that maintains Java software in Debian.
   [DebianMed](https://wiki.debian.org/DebianMed),
   [DebianScience](https://wiki.debian.org/DebianScience) and the [Android Tools
   Maintainers](https://wiki.debian.org/AndroidTools) rely heavily on Java. By
   helping the Java Team and working together, you can improve the Java ecosystem
   and further the efforts of multiple other fields of endeavor all at once.

## Package updates

The packages listed below detail the changes in jessie-backports and testing.
Libraries and Debian specific tools have been excluded.

Packages added to jessie-backports:

 * [ant](https://tracker.debian.org/pkg/ant) (1.9.7)
 * [elasticsearch](https://tracker.debian.org/pkg/elasticsearch) (1.6.2)
 * [gradle](https://tracker.debian.org/pkg/gradle) (2.10)
 * [groovy2](https://tracker.debian.org/pkg/groovy2) (2.4.5)
 * [japi-compliance-checker](https://tracker.debian.org/pkg/japi-compliance-checker) (1.5)
 * [jedit](https://tracker.debian.org/pkg/jedit) (5.3.0)
 * [jetty8](https://tracker.debian.org/pkg/jetty8) (8.1.19)
 * [jetty9](https://tracker.debian.org/pkg/jetty9) (9.2.14)
 * [maven](https://tracker.debian.org/pkg/maven) (3.3.9)
 * [openjdk-7-jre-dcevm](https://tracker.debian.org/pkg/openjdk-7-jre-dcevm) (7u79)
 * [openjdk-8](https://tracker.debian.org/pkg/openjdk-8) (8u72-b15)
 * [openjfx](https://tracker.debian.org/pkg/openjfx) (8u60-b27)
 * [tomcat7](https://tracker.debian.org/pkg/tomcat7) (7.0.69)
 * [tomcat8](https://tracker.debian.org/pkg/tomcat8) (8.0.32)

Packages removed from testing:

 * [avian](https://tracker.debian.org/pkg/avian)
 * [clojure1.2](https://tracker.debian.org/pkg/clojure1.2)
 * [clojure1.4](https://tracker.debian.org/pkg/clojure1.4)
 * [eclipse-cdt](https://tracker.debian.org/pkg/eclipse-cdt)
 * [eclipse-cdt-pkg-config](https://tracker.debian.org/pkg/eclipse-cdt-pkg-config)
 * [eclipse-egit](https://tracker.debian.org/pkg/eclipse-egit)
 * [eclipse-linuxtools](https://tracker.debian.org/pkg/eclipse-linuxtools)
 * [eclipse-mercurialeclipse](https://tracker.debian.org/pkg/eclipse-mercurialeclipse)
 * [eclipse-mylyn](https://tracker.debian.org/pkg/eclipse-mylyn)
 * [eclipse-mylyn-tasks-github](https://tracker.debian.org/pkg/eclipse-mylyn-tasks-github)
 * [eclipse-ptp](https://tracker.debian.org/pkg/eclipse-ptp)
 * [eclipse-subclipse](https://tracker.debian.org/pkg/eclipse-subclipse)
 * [eclipse-wtp](https://tracker.debian.org/pkg/eclipse-wtp)
 * [freemind](https://tracker.debian.org/pkg/freemind)
 * [groovy](https://tracker.debian.org/pkg/groovy)
 * [maven2](https://tracker.debian.org/pkg/maven2)
 * [openjdk-7](https://tracker.debian.org/pkg/openjdk-7)
 * [openjdk-7-jre-dcevm](https://tracker.debian.org/pkg/openjdk-7-jre-dcevm)

Packages added to testing:

 * [apache-directory-server](https://tracker.debian.org/pkg/apache-directory-server) (2.0.0~M15)
 * [dokujclient](https://tracker.debian.org/pkg/dokujclient) (3.8.1)
 * [elasticsearch](https://tracker.debian.org/pkg/elasticsearch) (1.7.3)
 * [ivyplusplus](https://tracker.debian.org/pkg/ivyplusplus) (1.14)
 * [jetty9](https://tracker.debian.org/pkg/jetty9) (9.2.16)
 * [netbeans](https://tracker.debian.org/pkg/netbeans) (8.1)
 * [openjdk-8](https://tracker.debian.org/pkg/openjdk-8) (8u91-b14)
 * [openjdk-8-jre-dcevm](https://tracker.debian.org/pkg/openjdk-8-jre-dcevm) (8u74)
 * [openjfx](https://tracker.debian.org/pkg/openjfx) (8u60-b27)

Packages upgraded in testing:

 * [activemq](https://tracker.debian.org/pkg/activemq) (5.13.2)
 * [ant](https://tracker.debian.org/pkg/ant) (1.9.7)
 * [aspectj](https://tracker.debian.org/pkg/aspectj) (1.8.9)
 * [bnd](https://tracker.debian.org/pkg/bnd) (2.4.1)
 * [checkstyle](https://tracker.debian.org/pkg/checkstyle) (6.15)
 * [eclipse-gef](https://tracker.debian.org/pkg/eclipse-gef) (3.9.100)
 * [electric](https://tracker.debian.org/pkg/electric) (9.06)
 * [felix-main](https://tracker.debian.org/pkg/felix-main) (5.0.0)
 * [findbugs](https://tracker.debian.org/pkg/findbugs) (3.0.1)
 * [fop](https://tracker.debian.org/pkg/fop) (2.1)
 * [freeplane](https://tracker.debian.org/pkg/freeplane) (1.3.15)
 * [gant](https://tracker.debian.org/pkg/gant) (1.9.11)
 * [gradle](https://tracker.debian.org/pkg/gradle) (2.10)
 * [groovy2](https://tracker.debian.org/pkg/groovy2) (2.4.5)
 * [hsqldb](https://tracker.debian.org/pkg/hsqldb) (2.3.3)
 * [icedtea-web](https://tracker.debian.org/pkg/icedtea-web) (1.6.2)
 * [ivy](https://tracker.debian.org/pkg/ivy) (2.4.0)
 * [jajuk](https://tracker.debian.org/pkg/jajuk) (1.10.9)
 * [jakarta-jmeter](https://tracker.debian.org/pkg/jakarta-jmeter) (2.13)
 * [japi-compliance-checker](https://tracker.debian.org/pkg/japi-compliance-checker) (1.7)
 * [jasmin-sable](https://tracker.debian.org/pkg/jasmin-sable) (2.5.0)
 * [java-common](https://tracker.debian.org/pkg/java-common) (0.57)
 * [java-package](https://tracker.debian.org/pkg/java-package) (0.61)
 * [jedit](https://tracker.debian.org/pkg/jedit) (5.3.0)
 * [jetty8](https://tracker.debian.org/pkg/jetty8) (8.1.19)
 * [jftp](https://tracker.debian.org/pkg/jftp) (1.60)
 * [jgit](https://tracker.debian.org/pkg/jgit) (3.7.1)
 * [jruby](https://tracker.debian.org/pkg/jruby) (1.7.22)
 * [jtreg](https://tracker.debian.org/pkg/jtreg) (4.2-b01)
 * [libapache-mod-jk](https://tracker.debian.org/pkg/libapache-mod-jk) (1.2.41)
 * [maven](https://tracker.debian.org/pkg/maven) (3.3.9)
 * [nailgun](https://tracker.debian.org/pkg/nailgun) (0.9.1)
 * [pleiades](https://tracker.debian.org/pkg/pleiades) (1.6.0)
 * [proguard](https://tracker.debian.org/pkg/proguard) (5.2.1)
 * [robocode](https://tracker.debian.org/pkg/robocode) (1.9.2.5)
 * [sablecc](https://tracker.debian.org/pkg/sablecc) (3.7)
 * [scala](https://tracker.debian.org/pkg/scala) (2.11.6)
 * [service-wrapper-java](https://tracker.debian.org/pkg/service-wrapper-java) (3.5.26)
 * [simplyhtml](https://tracker.debian.org/pkg/simplyhtml) (0.16.13)
 * [svnkit](https://tracker.debian.org/pkg/svnkit) (1.8.12)
 * [sweethome3d-textures-editor](https://tracker.debian.org/pkg/sweethome3d-textures-editor) (1.4)
 * [tomcat-native](https://tracker.debian.org/pkg/tomcat-native) (1.1.33)
 * [tomcat7](https://tracker.debian.org/pkg/tomcat7) (7.0.69)
 * [tomcat8](https://tracker.debian.org/pkg/tomcat8) (8.0.32)
 * [triplea](https://tracker.debian.org/pkg/triplea) (1.8.0.9)
 * [uimaj](https://tracker.debian.org/pkg/uimaj) (2.8.1)
 * [weka](https://tracker.debian.org/pkg/weka) (3.6.13)
 * [zookeeper](https://tracker.debian.org/pkg/zookeeper) (3.4.8)
