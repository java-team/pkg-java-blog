Debian Java Blog
----------------

This repository contains the sources needed to maintain the Debian Java blog
http://java.debian.net/blog

The blog uses the Pelican static site generator (apt-get install pelican).


Updating the blog
-----------------

1. Copy and rename the template.md file at the root of the repository
   into content/<year>/

2. Edit the content of the file

3. Push the changes to the Git repository on Salsa

4. Ask the other team members to review the article

5. Change the status of the article from 'draft' to 'published'

6. Commit and push the final changes to Salsa

7. Wait for the Gitlab job to regenerate the site
