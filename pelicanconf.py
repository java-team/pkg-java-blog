#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

# Basic details
AUTHOR = u'Debian Java Maintainers'
SITENAME = u'Debian Java News'
SITEURL = 'http://java.debian.net/blog'

# Configuration
PATH = 'content'
STATIC_PATHS = [ 'images' ]
TIMEZONE = 'Europe/Paris'
DEFAULT_LANG = u'en'
DELETE_OUTPUT_DIRECTORY = True
THEME = "theme-bits"
DEFAULT_PAGINATION = 10
SUMMARY_MAX_LENGTH = None
LOCALE='C'
USE_FOLDER_AS_CATEGORY = False

# URL settings
ARTICLE_URL = '{date:%Y}/{date:%m}/{slug}.html'
ARTICLE_SAVE_AS = '{date:%Y}/{date:%m}/{slug}.html'

# Feed settings
FEED_DOMAIN = SITEURL
FEED_ALL_RSS = 'feed.rss'
FEED_ALL_ATOM = 'atom.xml'
CATEGORY_FEED_ATOM = None
CATEGORY_FEED_RSS = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Links
MENUITEMS =  (('Home', 'http://java.debian.net/blog'),)

LINKS = (('Mailing List', 'https://lists.debian.org/debian-java/'),
         ('IRC Channel', 'irc://irc.debian.org/debian-java'),
         ('TODO List', 'https://udd.debian.org/dmd.cgi?email1=pkg-java-maintainers%40lists.alioth.debian.org#todo'),
         ('Packages overview', 'https://qa.debian.org/developer.php?email=pkg-java-maintainers%40lists.alioth.debian.org'),
         ('Reproducible Builds', 'https://reproducible.debian.net/unstable/amd64/pkg_set_maint_pkg-java-maintainers.html'),)

# Social widget
#SOCIAL = (('You can add links in your config file', '#'),
#          ('Another social link', '#'),)


# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True
