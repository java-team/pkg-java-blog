Title: This is my awesome news item
Date: YYYY-MM-DD HH:MM
Author: Name Surname(s)
Status: draft


We're excited to announce my awesome news item about [the Debian Project](https://www.debian.org)
and for that I'm writing this blog post in Markdown.

My second parapgrah has text attributes in *italic*, **bold** and `monospace`.

And I want to add a list:

 * foo
 * bar
 * baz

## Subtitle

> This is a quote

    :::Java
    /**
     * This is a code sample
     */
    public static void main(String[] argv) throws Exception {
        System.out.println("Hello World!");
    }

<center>![Debian Logo]({filename}/images/openlogo-nd-75.png)</center>

This is my last paragraph.
